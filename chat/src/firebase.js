import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyC7NKKOpUsakRvGMj7GQV_3SjEhbKqGjEk",
    authDomain: "chat-d6d7d.firebaseapp.com",
    projectId: "chat-d6d7d",
    storageBucket: "chat-d6d7d.appspot.com",
    messagingSenderId: "104217254191",
    appId: "1:104217254191:web:11b38c4789e01a02eea9da"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore()
const auth = firebase.auth()
const provider = new firebase.auth.GoogleAuthProvider()

export {db, auth, provider}