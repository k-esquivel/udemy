import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyCyakT-OPzBnobwp6pO0iqMlOvKy_p8I5U",
    authDomain: "pokemones-fb054.firebaseapp.com",
    projectId: "pokemones-fb054",
    storageBucket: "pokemones-fb054.appspot.com",
    messagingSenderId: "1033272779974",
    appId: "1:1033272779974:web:d521a5002d893e582c23c7"
  };

  
  firebase.initializeApp(firebaseConfig)
  const auth = firebase.auth(  )
const db = firebase.firestore()
const storage = firebase.storage()

  export{auth, firebase, db, storage}